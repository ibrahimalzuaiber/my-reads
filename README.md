## About The Project

-- MyReads Web application
-- An application used to manage your books online 


## Project Purpose:

The purpose of the project is to understanding of the basic structure using React


## Installation and Setup Instructions

Clone down this repository. You will need `node` and `npm` installed globally on your machine.  

Installation:

`npm install`  

To Start Server:

`npm start`  

To Visit App:

`localhost:3000`  
