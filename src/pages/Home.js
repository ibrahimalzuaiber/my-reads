import React, { Component } from 'react'
import List from '../components/List'
import * as BooksAPI from '../BooksAPI';
import { Route, Switch } from 'react-router-dom';
import Search from './../components/Search';
import NotFound from './../components/NotFound';
import Header from './../components/Header';
import Footer from './../components//Footer';


class Home extends Component {
  constructor() {
    super();
    this.state = { books: [] };
  }

   componentDidMount() {
     BooksAPI.getAll().then(books => this.setState({ books })); 
  }

  changeShelf = (changed, shelf) => {
    BooksAPI.update(changed, shelf).then(() => {
      changed.shelf = shelf;
      this.setState(prevState => ({
        books: prevState.books.filter(book => book.id !== changed.id).concat(changed)
      }));
    });
    alert(window.location.pathname === '/search'? 'The book has been added successfully': 'The shelf of book has been changed successfully');
  };

  render() {
    const { books } = this.state;

    return (
      <div className="app">
      <Switch>
        <Route
          path="/search"
          render={() => (
            <Search books={books} changeShelf={this.changeShelf} />
          )}
        />
        <Route
          exact
          path="/"
          render={() => (
            <div className="list-books">
              <Header/>
              <List books={books} changeShelf={this.changeShelf} />
              <Footer/>
            </div>
          )}
        />
        <Route component={NotFound} />
      </Switch>
    </div>
    )
  }
}

export default Home;