import React from 'react'
import PropTypes from 'prop-types'
import Shelf from './Shelf';

const List = props => {
  const { books, changeShelf } = props;
  const types = [
      { type: 'currentlyReading', title: 'Currently Reading' },
      { type: 'wantToRead', title: 'Want to Read' },
      { type: 'read', title: 'Read' },
      
  ];
  return (
    <div className="list-books-content">
         {types.map((name, index) => {
            const Books = books.filter(book => book.shelf === name.type);
            return(
              <div className="bookshelf" key={index}>
                <h2 className="bookshelf-title">{name.title}</h2>
                <div className="bookshelf-books">
                  <Shelf books={Books} changeShelf={changeShelf} />
                </div>
              </div>
            );
         })}
    </div>
  )
}

List.propTypes = {
    books: PropTypes.array.isRequired,
    shelfChanged: PropTypes.func.isRequired
}

export default List