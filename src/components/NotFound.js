import React from 'react'
import { Link } from 'react-router-dom'

const NotFound = () => {
  return (
    <div className="not-found">
        <h1 className="not-found-title"> We were not able to find the page you're looking for.</h1>
        <div className="home-link">
            <div> Don’t worry, it’s not the end of the world. Simply <Link to="/">Click Here</Link> to go to the home page</div>
        </div>
    </div>
  )
}


export default NotFound